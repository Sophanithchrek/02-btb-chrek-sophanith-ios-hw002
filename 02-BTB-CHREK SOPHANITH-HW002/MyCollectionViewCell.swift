//
//  MyCollectionViewCell.swift
//  02-BTB-CHREK SOPHANITH-HW002
//
//  Created by SOPHANITH CHREK on 23/11/20.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userProfileStoryImageView: UIImageView!
    @IBOutlet weak var lblUsernameStory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
