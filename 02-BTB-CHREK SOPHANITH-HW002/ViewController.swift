//
//  ViewController.swift
//  02-BTB-CHREK SOPHANITH-HW002
//
//  Created by SOPHANITH CHREK on 23/11/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    var myObj = MyStruct(userProfile: ["Captain-Marvel", "Captain-America", "Thanus", "Captain-Marvel", "Captain-America", "Thanus", "Captain-America"], userName: ["John", "Smith", "Steve", "John", "Smith", "Steve", "Peter"], mainImageViewLayout: ["spider-man1", "spider-man2", "spider-man3", "spider-man1", "spider-man2", "spider-man3", "spider-man2"], lblLike: ["10000 Likes", "2000 Likes", "3000 Likes", "40000 Likes", "5000 Likes", "6000 Likes", "7000 Likes"], lblDescription: ["Have a nice day", "Best luck", "Happy Birthday", "Have a nice day", "Best luck", "Happy Birthday", "Have a nice day"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.dataSource = self
        myTableView.delegate = self
        myCollectionView.dataSource = self
        
        // Set image to UINavigationItem
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        // Register to UINib of UITableViewCell
        myTableView.register(UINib(nibName: "MyTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        // Register to UINib of UICollectionViewCell
        myCollectionView.register(UINib(nibName: "MyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myObj.userProfile.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTableViewCell
        cell.userProfileImageView.image = UIImage(named: myObj.userProfile[indexPath.row])
        cell.lblUsername.text = myObj.userName[indexPath.row]
        cell.mainLayoutImageView.image = UIImage(named: myObj.mainImageViewLayout[indexPath.row])
        cell.lblUserLiked.text = myObj.lblLike[indexPath.row]
        cell.lblUserDescription.text = myObj.lblDescription[indexPath.row]
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420
    }
}

// UICollectionView
extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myObj.userProfile.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCollectionViewCell
        cell.userProfileStoryImageView.image = UIImage(named: myObj.userProfile[indexPath.row])
        cell.lblUsernameStory.text = myObj.userName[indexPath.row]
        return cell
        
    }
    
}

