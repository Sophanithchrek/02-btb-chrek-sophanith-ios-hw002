//
//  MyTableViewCell.swift
//  02-BTB-CHREK SOPHANITH-HW002
//
//  Created by SOPHANITH CHREK on 23/11/20.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonHeart: UIButton!
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var mainLayoutImageView: UIImageView!
    @IBOutlet weak var lblUserLiked: UILabel!
    @IBOutlet weak var lblUserDescription: UILabel!
    
    @IBOutlet weak var txtUserInputComment: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var isChanged: Bool = false
    @IBAction func buttonHeartPressed(_ sender: UIButton) {
        if isChanged == false {
            buttonHeart.tintColor = .red
            buttonHeart.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            isChanged = true
        } else {
            buttonHeart.tintColor = .black
            buttonHeart.setImage(UIImage(systemName: "heart"), for: .normal)
            isChanged = false
        }
    }
    
    @IBAction func buttonChatPressed(_ sender: UIButton) {
        txtUserInputComment.returnKeyType = .done
        txtUserInputComment.becomeFirstResponder()
    }
    
}
