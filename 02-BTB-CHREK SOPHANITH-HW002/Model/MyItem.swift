//
//  MyItem.swift
//  02-BTB-CHREK SOPHANITH-HW002
//
//  Created by SOPHANITH CHREK on 23/11/20.
//

import UIKit

struct MyStruct {
    var userProfile: [String]
    var userName: [String]
    var mainImageViewLayout: [String]
    var lblLike: [String]
    var lblDescription: [String]
}

